import './App.scss';
import CategoryCard from './CategoryCard';

function App() {
  return (
    <div className="QuotePulpe">
      <header>
        Pulpe. Event.
      </header>
      <form className="quote-form">
        <CategoryCard title="Pasta" />
      </form>
    </div>
  );
}

export default App;
