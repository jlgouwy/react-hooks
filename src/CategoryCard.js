import { useEffect } from 'react';
import QuantityField from './QuantityField';
import { set as _set } from 'lodash/object';

function CategoryCard({title}) {
  useEffect(() => {
    localStorage.setItem('quantities', JSON.stringify({
      'food': {
        'pasta': {
          'small': 0,
          'medium': 0
        }
      }
    }));
  }, []);

  const handleQty = (product, newQty) => {
    let quantities = JSON.parse(localStorage.getItem('quantities'));
    _set(quantities, product, newQty);
    localStorage.setItem('quantities', JSON.stringify(quantities));
  }

  return (
    <div className="cat-qty-card">
      <div className="cat-title">{title}</div>
      <QuantityField title="Small" displayPrice="3,3€/kg" name="food.pasta.small" onChange={handleQty} />
      <QuantityField title="Medium" displayPrice="3,3€/kg" name="food.pasta.medium" onChange={handleQty} />
    </div>
  );
}

export default CategoryCard;
