import { useRef, useState, useEffect } from 'react';

function QuantityField({ title, displayPrice, name, value, onChange }) {
  const qtyInput = useRef(null);
  const [ quantity, setQuantity ] = useState(() => {
    return value === undefined ? 0 : value;
  });

  useEffect(() => {
    onChange(quantity === '' ? 0 : quantity);
  }, [onChange, quantity]);

  console.log('render APP', name, quantity);

  const handleQty = (event) => {
    setQuantity(event.target.value);
  }

  const increment = (event) => {
    event.preventDefault();
    const newQty = quantity + 1;

    setQuantity(newQty);
  }

  const decrement = (event) => {
    event.preventDefault();
    const newQty = quantity - 1;

    setQuantity(newQty);
  }
  return (
    <div className="qty-card">
      <div className="qty-info">
        <span>{title}</span>
        <span>{displayPrice}</span>
      </div>
      <div className="qty-counter-wrapper">
        <div className="qty-counter">
          <button onClick={increment}>+</button>
          <input ref={qtyInput} type="text" value={quantity} onChange={handleQty} />
          <button onClick={decrement}>-</button>
        </div>
        
      </div>
    </div>
  );
}

export default QuantityField;
